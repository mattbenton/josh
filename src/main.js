var log = console.log.bind(console);

log("step 1");

// Vue.component("bob", {
//   replace: true,
//   template: '<div v-class="bad: age > 2">This is Bob, he is {{ age }}</div>',
//   data: function () {
//     return {
//       age: Math.random() * 10
//     };
//   }
// });

var goaltableApp = angular.module('goaltableApp', []);

goaltableApp.controller('GoaltableCtrl', function ($scope, $http) {
  var id = 70405;
  loadData(id);
  setTimeout(function () {loadData(70411);}, 2000);

  // $scope.players = [{name:'test'}, {name:'foo'}];
  
  
  function loadData ( id ) {
    log("loadData", id);
    $http.get("http://localhost/handball/hb_joomla3/index.php?option=com_hbteam&task=getGoals2&format=raw&gameId=" + id + "&season=2014&teamkey=m-1")
     .then(function(res){
        $scope.players = res.data.player;
        log($scope.players);                
      });
  }
  
});

  
// var vm = new Vue({
//   el: "#app",
//   data: {
//     title: "matt <b>is</b> cool",
//     players: []
//   },
//   ready: function () {
//     vm = this;
//     log("ready", vm);
//     loadData(70411);
//   },
//   methods: {
//     update: function ( id ) {
//       log("update", id);
//       // loadData(id);
//     }
//   }
// });

// // window.matt = vm;

// // log("step 2");


